---
title: "les backdoors : un problème de compilation ou d'audit ? 3/3 Quelqu'un a relu le code récemment ?"
date: "2024-05-27"
categories: 
   - "sécurité"
   - "reproductibilité"
description: "Fin de notre cycle sur les vulnérabilités du code source. On l'a vu, la cryptographie aide à s'assurer que le logiciel -censé garantir notre sécurité- qu'on s'apprête à télécharger sur tel ou tel site est bien loyal et ne comporte aucune backdoor (épisode 1). Sauf que pour que cela fonctionne, il faut être capable de compiler des sources de manière reproductible afin que la somme de l'exécutable téléchargé soit bien identique à celle fournie dans le certificat livré avec le produit compilé (épisode 2). Une erreur courante consisterait à croire que le Diable se cache dans le compilateur ou sur le serveur depuis lequel on télécharge l'exécutable. Pourtant, des exemples plus ou moins récents, dont le retentissant épisode autour d'XZUtils, montre qu'à l'instar de la lettre volée de Poe, la faille souvent ne se trouve pas dans un processus de compilation qu'on ne maîtrise pas, mais dans le code source lui-même : une lettre volée qui en théorie peut-être décachetée par tout le monde, mais qui en réalité ne livre son sens qu'au très petit nombre de celles et ceux à qui ne manquent ni les compétences nécessaires pour la lire, ni - en l'absence de reconnaissance extérieure pour cette tâche, la motivation intrinsèque et l'esprit de responsabilité."
---
![Mathias Passtwa, Backdoor on Flickr, CC-by](images/backdoor.jpg)


Comme la communauté des développeurs reste très inférieure en nombre à celle des utilisateurs du logiciel libre et que cette communauté ne peut prendre en charge collaborativement la révision du code livré, il arrive que le logiciel libre -présenté comme une option plus sûre que le logiciel propriétaire- conserve néanmoins des vulnérabilités très longtemps, parfois plusieurs années avant qu'une personne suffisamment motivée et compétente n'arrive à les détecter. Dans un précédent post, j'ai déjà cité la faille *Heartbleed* qui rendait le protocole openSSL vulnérable. On peut ajouter à cette liste la vulnarabilité découverte dans le programme *log4shell* qui est lié au logiciel log4j de Java. Cette faille a été [publiée en 2021](https://fr.wikipedia.org/wiki/Log4Shell)). 

Dix ans après Heartbleed, XZ, un outil de compression faisant partie (dépendance) du protocole OpenSSH que peu connaissent mais que tout le monde utilise met le monde du Libre en émoi. Cette library n'était plus maintenue que par son créateur, Lasse Collin, qui l'avait au départ développée comme un "projet bénévole et de loisir" et avait progressivement laissé la maintenance à un énigmatique Jia Tan, en expliquant à la communauté que cette décision était due à un "problème mental" qui ne lui permettait plus de mettre le code à jour [^1]. Jia Tan s'avèrera un attaquant très méthodique et patient puisqu'il est entré en contact avec Collin dès octobre 2021.

Il est impossible à l'heure actuelle de prouver que le dénommé Jia Tan est bien chinois et que son action correspond bien à une action d'infiltration qui s'est déroulée sur plusieurs années au profit d'une puissance étrangère, même si cette dernière hypothèse semble hautement probable. Au terme d'une coopération de plus en plus substantielle, Lasse Collin finit donc par donner à Jia Tan la possibilité de merger le code existant avec le sien. Or dans sa version, Jia Tan a ajouté une porte dérobée (*backdoor*) qui permet au flux d'information compressé par cet utilitaire d'être également dérivé vers d'autres serveurs non autorisés. Cette faille a été détectée de justesse entre le moment où la dernière version d'OpenSSL avec le code de Jia Tan a été livré et le moment où cette version allait être intégrée à la un très grand nombre de serveurs dans le monde.

C'est le constat que la nouvelle version de XZ réalisait la compression avec 500 millisecondes de plus que dans la version précédente qui a mis la puce à l'oreille d'un utilisateur attentif, Andres Freund. Freund s'empresse d'informer l'équope de développement de Debian qui stoppe la compilation de cette version au niveau de ses propres serveurs et avertit sa communauté d'utilisateurs. 
Après quoi l'information sur la compromission d'XZ fait le tour de la toile et génère 6 fois plus de posts sur Mastodon que sur Twitter (ce qui montre au passage qu'il n'y a guère d'intérêt à rester dans ce feu de poubelle qu'est devenu X/Twitter quand on travaille dans le monde du développement ou de la sécurité).
Toute la chronologie de cette faille [peut-être consultée en ligne](https://research.swtch.com/xz-timeline?eicker.news) grâce aux efforts concertés de témoins compétents.

Le problème ici est le même pour cURL ; des composants libres essentiels pour faire fonctionner une majorité de sites ou de serveurs ne sont maintenus que par une poignée d'individus qui sont bientôt dépassés par cette tâche. On peut trouver certains acariâtres ou peu réactifs, ils sont surtout surmenés et leur situation personnelle constitue une vulnérabilité de grande ampleur pour l'ensemble du web.
Pour faire face à cette situation, il apparaît de plus en plus indispensable que les institutions publiques qui promovent le logiciel libre ou les entreprises qui réutilisent ces composants libres prennent leur part de responsabilité et viennent en aide à ces personnes. 

Combien de Jia Tan sont en train peu à peu, jour après jour d'infiltrer les maigres communautés qui développent ces briques essentielles à l'ensemble de l'édifice, un édifice sur lequel reposent les bénéfices mirobolants des Géants de la Tech, un édifice qui s'échafaude sur le travail gratuit d'une armée de bénévoles à la fois passionnés et dépassés (ils doivent gagner leur vie par ailleurs) ?

Tim Bryan a proposé la création dans les pays développés d'instituts dédiés à la qualité du code ouvert ([OSQI](https://www.tbray.org/ongoing/When/202x/2024/04/01/OSQI)) et d'y financer des développeurs fonctionnaires chargés de la révision du code libre. Cette proposition paraît peu réaliste à ce stade. 

Si déjà la force publique pouvait éviter d'avoir une vision néo-libérale du code source ouvert et cessait d'imposer aux développeurs de logiciels libres qui oeuvrent pour elle une mise en concurrence et des contrats qui les excluent de la gouvernance du produit, ce serait un très bon début [^2]. 

L'idée qui revient le plus souvent vise à juste titre les multinationales du logiciel qui rechignent à prendre en charge, à la mesure de leurs moyens considérables, les coûts de la maintenance et de la vérification des composants libres essentiels qu'ils utilisent dans leurs logiciels. 

Cette proposition d'un financement contraint que Stéphane Bortzmeyer qualifie d'[impôt sur le capitalisme](https://twitter.com/bortzmeyer/status/1775449068971438513) a peu de probabilité de voir le jour. [Un autre Mastonaute](https://status.pointless.one/@pointlessone/112202804564535106) ne reprend pas l'idée d'une taxte mais d'une protection accrue des travailleurs qui veillent à l'intégrité et l'innocuité du code source produit ou réutilisé par les entreprises de la Tech : il ne devrait plus être possible de rémunérer les mainteneurs à la tâche mais uniquement avec des contrats qui leur assurent un mode de vie moins sujet à la précarité.

Comme dans mon précédent post sur les [compilations reproductibles](https://site.dbelveze.fr/posts/reproducible_builds.html), un problème de sécurité lié au logiciel libre pose une question identique dans le monde de la recherche : si je livre à la communauté des chercheurs un code source que j'ai conçu pour traiter un jeu de données, aurai-je moi-même assez de temps -sachant que je ne suis pas développeur mais chercheur- pour veiller à la maintenance de ce code ? Et si j'y associe des développeurs bénévoles qui désirent m'aider à cette tâche, motivés par l'envie de faire évoluer mon script en fonction de leurs besoins, comment vais-je pouvoir m'assurer de conserver au moins une part de la gouvernance globale du projet ?





_____________________

[^1]:[comme l'atteste un message archivé envoyé par Lasse Collin à la communauté le 7 juin 2022](https://www.mail-archive.com/xz-devel@tukaani.org/msg00567.html)
[^2]: [1] C. Masutti, « Communs numériques et souveraineté : sauver les logiciels libres », La Vie des idées, juin 2024, Consulté le: 26 juin 2024. [En ligne]. Disponible sur: https://laviedesidees.fr/Communs-numeriques-et-souverainete-sauver-les-logiciels-libres
